import React from 'react';
import { expect } from 'chai';
import Enzyme from 'enzyme';
import { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import "isomorphic-fetch";
Enzyme.configure({ adapter: new Adapter() });

import Home from '../src/pages/home';

describe('IndexContent Component', () => {
  it('contains Jumbotron', () => {
    const components = shallow(<Home />);
    expect(components.find('Jumbotron'));
  })
  it('contains carousel slide', () => {
    const components = shallow(<Home />);
    expect(components.find('Carousel'));
  })
  it('contains Carousel Inner', () => {
    const components = shallow(<Home />);
    expect(components.find('Carousel.Item'));
  })
})
