import React from 'react';
import { expect } from 'chai';
import Enzyme from 'enzyme';
import { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import "isomorphic-fetch";
Enzyme.configure({ adapter: new Adapter() });

import Navigation from '../src/pages/Navigation';

describe('Navigation Component', () => {
  it('contains Navbar', () => {
    const components = shallow(<Navigation />);
    expect(components.find('Navigation'));
  })
  // it('contains container', () => {
  //   const components = mount(<Navigation />);
  //   expect(components.hasClass('navbar-brand'));
  // })
})
