import React from 'react';
import { expect } from 'chai';
import Enzyme from 'enzyme';
import { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import "isomorphic-fetch";
Enzyme.configure({ adapter: new Adapter() });

import App from '../src/App';
var assert = require('assert');

describe('App Components & classes', () => {
  assert.equal(1, 0)

  it('contains a Router Component', () => {
    const about = shallow(<App />)
    expect(about.find('Router'))
  })
  
  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('Navigation'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('Home'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('Energy'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('EnergyInstance'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('Production'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('ProductionInstance'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('Country'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('CountryInstance'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('About'))
  })

  it('contains a Component', () => {
    const about = shallow(<App />)
    expect(about.find('Footer'))
  })
})
